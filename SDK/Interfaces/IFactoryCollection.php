<?php


namespace Axium\SDK\Interfaces;


interface IFactoryCollection
{
    /**
     * @return IRepositoryFactory
     */
    public function RepositoryInstance();

    /**
     * @return IUtilityFactory
     */
    public function UtilityInstance();
}
<?php


namespace Axium\SDK\Interfaces;

use Axium\SDK\Models\RepositoryModel;
use Axium\SDK\Models\ResponseModel;

interface IResponse
{
    public function __construct(ResponseModel $model);

    /**
     * @return int
     */
    public function GetStatus();

    /**
     * @return string[]
     */
    public function ErrorList();

    /**
     * @param int $id index of the record to be returned (1 being the first index)
     * @return object
     */
    public function GetRecord($id=1);

    /**
     * @return object[]
     */
    public function GetRecordList();

    /**
     * @param string $key identifies the parameter to be used as the value
     * @param string $val identifies the parameter to be used as the text
     * @return object[] returns an array of key value objects eg. [{value:'',text:''},{value:'',text:''}]
     */
    public function GetDropDownList($key='ID',$val='Name');

    /**
     * @return int
     */
    public function GetRecordCount();

    /**
     * @return string
     */
    public function Serialize();

    /**
     * @param string $result serialized response from api call
     * @param object $model model that should be used when converting the response to an object
     * @param array $mapper if model parameters are different from the response result... add a mapper
     * @return ResponseModel
     */
    public static function UnPack($result,$model,$mapper=null);
}
<?php


namespace Axium\SDK\Interfaces;


interface IProcessor
{
    /**
     * @return object
     */
    public function GetModel();

    /**
     * @return string
     */
    public function GetModelAndSerialize();

    /**
     * @return array
     */
    public function GetCollection();

    /**
     * @return string
     */
    public function GetCollectionAndSerialize();

    /**
     * @param string $name name used to identify and map model to viewmodel
     * @param object $model populated model to be loaded in
     * @return IProcessor
     */
    public function AddToViewModel($name,$model);

    /**
     * @return string gets the error message from the top of the stack
     */
    public function GetNextError();

    /**
     * @return int gets the depth of the error stack
     */
    public function ErrorCount();

    /**
     * @return string[] returns the errors on the stack
     */
    public function GetAllErrors();
}
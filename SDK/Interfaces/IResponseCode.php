<?php

namespace Axium\SDK\Interfaces;


interface IResponseCode
{
    const OK = 200;
    const NOT_MODIFIED = 300;
    const NOT_FOUND = 400;
    const INTERNAL_ERROR = 500;
    const NO_RESULTS = 600;

    /**
     * @param int $code response code
     * @return bool
     */
    public static function IsOk($code);

    /**
     * @param int $code response code
     * @return bool
     */
    public static function IsNotFound($code);

    /**
     * @param int $code response code
     * @return bool
     */
    public static function IsNotModified($code);

    /**
     * @param int $code response code
     * @return bool
     */
    public static function IsInternalError($code);

    /**
     * @param int $code response code
     * @return bool
     */
    public static function HasNoResult($code);

}
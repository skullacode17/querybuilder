<?php


namespace Axium\SDK\Interfaces;
use Axium\SDK\Models\DataSourceModel;
use Axium\SDK\Models\OrderModel;
use Axium\SDK\Models\PaginationModel;
use Axium\SDK\Models\RepositoryModel;

interface IRepository
{
    /**
     * @return DataSourceModel information about the repository
     */
    public function DataSource();

    /**
     * @return object the repository model associated with the instance
     */
    public function GetModel();
}
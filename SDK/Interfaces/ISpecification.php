<?php


namespace Axium\SDK\Interfaces;


interface ISpecification
{
    /**
     * @param string $username a supplied username
     * @param string $password a supplied password
     * @return IPredicate the generated predicate
     */
    public static function GetUserByCredentials($username,$password);

    /**
     * @param string $query an expression to use as the search criteria
     * @return IPredicate the generated predicate
     */
    public static function Search($query);
}
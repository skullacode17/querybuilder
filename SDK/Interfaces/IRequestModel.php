<?php


namespace Axium\SDK\Interfaces;


use Axium\SDK\Models\ResultModel;

interface IRequestModel
{
    /**
     * @return bool
     */
    public function IsValid();

    /**
     * @return string[]
     */
    public function ErrorList();

    /**
     * @param string $error error message
     * @return IRequestModel
     */
    public function AddError($error);

    /**
     * @param string $name identifier for the model that is added
     * @param object $model the model with the data
     * @return IRequestModel
     */
    public function AddModel($name,$model);

    /**
     * @param string $name identifier for the list of models added
     * @param object[] $list list of models
     * @return IRequestModel
     */
    public function AddList($name,$list);

    /**
     * @return object[]
     */
    public function ToArray();

    /**
     * @return string
     */
    public function GetResultAndSerialize();

    /**
     * @return ResultModel
     */
    public function GetResult();

    /**
     * @param bool $bool the status state
     * @return IRequestModel
     */
    public function SetSuccess($bool);

    /**
     * @param string $message the message to display
     * @return IRequestModel
     */
    public function SetMessage($message);
}
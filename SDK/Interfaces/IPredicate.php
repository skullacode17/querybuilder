<?php


namespace Axium\SDK\Interfaces;


interface IPredicate
{
    const STARTS_WITH = 1;

    const ENDS_WITH = 2;

    const CONTAINS = 3;

    /**
     * @param string $name the name of the field
     * @param string $operation the operation signature eg(=,<,>,etc...)
     * @param string $value the value to apply the operation to
     * @return IPredicate
     */
    public function Where($name, $operation, $value);

    /**
     * @param string $name the name of the field
     * @param string $operation the operation signature eg(=,<,>,etc...)
     * @param string $value the value to apply the operation to
     * @return IPredicate
     */
    public function OrWhere($name, $operation, $value);

    /**
     * @param string $name the name of the field
     * @param array $values the values to look through
     * @return IPredicate
     */
    public function In($name, $values);

    /**
     * @param string $name the name of the field
     * @param array $values the values to look through
     * @return IPredicate
     */
    public function OrIn($name, $values);

    /**
     * @param string $name the name of the field
     * @param array $values the values to look through
     * @return IPredicate
     */
    public function NotIn($name, $values);

    /**
     * @param string $name the name of the field
     * @param array $values the values to look through
     * @return IPredicate
     */
    public function OrNotIn($name, $values);

    /**
     * @param string $name the name of the field
     * @param int $const indicates how the comparison should take place
     * @param string $value the value on which the operation is executed
     * @return IPredicate
     */
    public function Like($name, $const, $value);

    /**
     * @param string $name the name of the field
     * @param int $const indicates how the comparison should take place
     * @param string $value the value on which the operation is executed
     * @return IPredicate
     */
    public function OrLike($name, $const, $value);

    /**
     * @param string $name the name of the field
     * @param int $const indicates how the comparison should take place
     * @param string $value the value on which the operation is executed
     * @return IPredicate
     */
    public function NotLike($name, $const, $value);

    /**
     * @param string $name the name of the field
     * @param int $const indicates how the comparison should take place
     * @param string $value the value on which the operation is executed
     * @return IPredicate
     */
    public function OrNotLike($name, $const, $value);

    /**
     * @param string $name the name of the field
     * @param string $operation the operation signature eg(=,<,>,etc...)
     * @param string $value the value to apply the operation to
     * @return IPredicate
     */
    public function Having($name, $operation, $value);

    /**
     * @param string $name the name of the field
     * @param string $operation the operation signature eg(=,<,>,etc...)
     * @param string $value the value to apply the operation to
     * @return IPredicate
     */
    public function OrHaving($name, $operation ,$value);

    /**
     * @param string $name the name of the field
     * @param string $val1 the first value of comparison
     * @param string $val2 the second value of comparison
     * @return IPredicate
     */
    public function Between($name, $val1, $val2);

    /**
     * @param string $name the name of the field
     * @param string $val1 the first value of comparison
     * @param string $val2 the second value of comparison
     * @return IPredicate
     */
    public function NotBetween($name, $val1, $val2);

    /**
     * @param string $name the name of the field
     * @param string $val1 the first value of comparison
     * @param string $val2 the second value of comparison
     * @return IPredicate
     */
    public function OrBetween($name, $val1, $val2);

    /**
     * @param string $name the name of the field
     * @param string $val1 the first value of comparison
     * @param string $val2 the second value of comparison
     * @return IPredicate
     */
    public function OrNotBetween($name, $val1, $val2);

    /**
     * @return IPredicate
     */
    public function StartGroup();

    /**
     * @return IPredicate
     */
    public function EndGroup();

    /**
     * Returns the generated expression in a string that can be parsed by
     * The repository.
     * @return string
     */
    public function PredicateExpression();
}
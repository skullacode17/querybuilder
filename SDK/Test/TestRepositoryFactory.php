<?php


namespace Axium\SDK\Test;

use Axium\SDK\Interfaces\IRepositoryFactory;

class TestRepositoryFactory implements IRepositoryFactory
{
    public function TestRepository()
    {
        return new TestRepository();
    }
}
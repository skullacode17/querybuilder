<?php


namespace Axium\SDK\Test;


use Axium\SDK\Interfaces\IFactoryCollection;
use Axium\SDK\Interfaces\IRepositoryFactory;
use Axium\SDK\Interfaces\IUtilityFactory;

class TestFactory implements IFactoryCollection
{

    /**
     * @return IRepositoryFactory
     */
    public function RepositoryInstance()
    {
        return new TestRepositoryFactory();
    }

    /**
     * @return IUtilityFactory
     */
    public function UtilityInstance()
    {
        return new TestUtilityFactory();
    }
}
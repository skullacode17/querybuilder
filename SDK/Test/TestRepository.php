<?php


namespace Axium\SDK\Test;
use Axium\SDK\Interfaces\IRepository;
use Axium\SDK\Models\DataSourceModel;
use Axium\SDK\Models\ResponseModel;
use Axium\SDK\Services\Utilities\Response;
use Axium\SDK\Services\Utilities\ResponseCode;
use Medoo\Medoo;

class TestRepository implements IRepository
{
    /**
     * @return DataSourceModel information about the repository
     */
    public function DataSource()
    {
        $db = new DataSourceModel();
        $db->Status = ResponseCode::OK;
        $db->Version = "1.0.0";
        $db->Type = "Database";
        return $db;
    }

    /**
     * @return object the repository model associated with the instance
     */
    public function GetModel()
    {
        return new TestRepositoryModel();
    }

    public function GetData()
    {
        $db = new Medoo([
            'database_type'     =>  'mysql',
            'database_name'     =>  'field',
            'server'            =>  'localhost',
            'username'          =>  'root'
        ]);

        $res = new ResponseModel();
        $res->Status = ResponseCode::OK;

        $data = $db->select('athletes','*');
        if(!empty($data))
        {
            foreach($data as $datum)
            {
                $x = new TestRepositoryModel();
                $x->ID = $datum['id'];
                //$x->Name = $datum['name'];
                $x->Color = $datum['first_name'];
                //$x->Crest = $datum['crest'];
                //$x->Mascot = $datum['mascot'];
                $res->Models[] = $x;
            }
        }
        return new Response($res);
    }
}
<?php


namespace Axium\SDK\Test;


class TestRepositoryModel
{
    public $ID;
    public $Name;
    public $Color;
    public $Crest;
    public $Mascot;
}
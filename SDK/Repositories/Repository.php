<?php


namespace Axium\SDK\Repositories;


use Axium\SDK\Interfaces\IRepository;
use Axium\SDK\Models\DataSourceModel;
use Axium\SDK\Services\Utilities\ResponseCode;

class Repository implements IRepository
{
    /**
     * @var object Stores the model that will be used to structure results
     */
    protected $Model;
    /**
     * @return DataSourceModel information about the repository
     */
    public function DataSource()
    {
        $data = new DataSourceModel();
        $data->Status = ResponseCode::OK;
        $data->Version = '';
        $data->Type = '';
        return $data;
    }

    /**
     * @return object the repository model associated with the instance
     */
    public function GetModel()
    {
        return $this->Model;
    }
}
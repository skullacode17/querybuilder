<?php


namespace Axium\SDK\Repositories;
use Axium\SDK\Interfaces\IPredicate;
use Tree\Node\Node;
use Tree\Node\NodeInterface;
use Tree\Visitor\YieldVisitor;

class Predicate implements IPredicate
{
    /**
     * @var Node[] list of trees as the predicate becomes nested
     */
    protected $Group;

    /**
     * @var Node main tree from which all trees branch
     */
    protected $Root;

    /**
     * @var int determines if nesting is taking place and what level
     */
    protected $GroupActive;

    /**
     * @var string Stores the string generated from parsing the expression tree
     */
    protected $PredicateString;

    /**
     * @param array $query builds the tree
     */
    protected function QueryBuilder($query)
    {
        // check to see if a group is active
        if($this->GroupActive > 0)
        {
            //if the group is null....first iteration... add the value to the node
            if(is_null($this->Group["$this->GroupActive"]))
            {
                $this->Group["$this->GroupActive"] = new Node($query['operation']);
            }
            // if the node is a leaf... we need to set the evaluation as the parent
            elseif($this->Group["$this->GroupActive"]->isLeaf())
            {
                $new_child = $this->Group["$this->GroupActive"]->getValue();
                $this->Group["$this->GroupActive"]->setValue($query['evaluation']);
                $this->Group["$this->GroupActive"]->addChild(new Node($new_child));
                $this->Group["$this->GroupActive"]->addChild(new Node($query['operation']));
            }
            //if node... add a stub that will be referenced on expression generation
            elseif($query['evaluation'] === 'node')
            {
                $this->Group["$this->GroupActive"]->addChild(new Node('NODE|'.$query['operation']));
            }
            // must be an evaluator..... we add a parent node
            else
            {
                $this->Group["$this->GroupActive"]->addChild(new Node($query['operation']));
            }
        }
        else
        {
            //same as group.... but we use the $this->Root as the Tree.
            if(is_null($this->Root))
            {
                $this->Root = new Node($query['operation']);
            }
            elseif($this->Root->isLeaf())
            {
                $new_child = $this->Root->getValue();
                $this->Root->setValue($query['evaluation']);
                $this->Root->addChild(new Node($new_child));
                $this->Root->addChild(new Node($query['operation']));
            }
            elseif($query['evaluation'] === 'node')
            {
                $this->Root->addChild(new Node('NODE|'.$query['operation']));
            }
            else
            {
                $this->Root->addChild(new Node($query['operation']));
            }
        }
    }

    /**
     * Predicate constructor.
     */
    public function __construct()
    {
        $this->Root = null;
        $this->Group = [];
        $this->GroupActive = 0;
        $this->PredicateString = '';
    }

    /**
     * @param string $name the name of the field
     * @param string $operation the operation signature eg(=,<,>,etc...)
     * @param string $value the value to apply the operation to
     * @return IPredicate
     */
    public function Where($name, $operation, $value)
    {
        $this->QueryBuilder([
            'evaluation'    =>  'AND',
            'operation'     =>  $name.' '.$operation.' '.$value
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param string $operation the operation signature eg(=,<,>,etc...)
     * @param string $value the value to apply the operation to
     * @return IPredicate
     */
    public function OrWhere($name, $operation, $value)
    {
        $this->QueryBuilder([
            'evaluation'    =>  'OR',
            'operation'     =>  $name.' '.$operation.' '.$value
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param array $values the values to look through
     * @return IPredicate
     */
    public function In($name, $values)
    {
        $operation = $name.' IN (';
        foreach($values as $v)
            $operation .= $v.',';
        $operation = rtrim($operation,',');
        $operation .= ')';
        $this->QueryBuilder([
            'evaluation'    =>  'AND',
            'operation'     =>  $operation
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param array $values the values to look through
     * @return IPredicate
     */
    public function OrIn($name, $values)
    {
        $operation = $name.' IN (';
        foreach($values as $v)
            $operation .= $v.',';
        $operation = rtrim($operation,',');
        $operation .= ')';
        $this->QueryBuilder([
            'evaluation'    =>  'OR',
            'operation'     =>  $operation
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param array $values the values to look through
     * @return IPredicate
     */
    public function NotIn($name, $values)
    {
        $operation = $name.' NOT IN (';
        foreach($values as $v)
            $operation .= $v.',';
        $operation = rtrim($operation,',');
        $operation .= ')';
        $this->QueryBuilder([
            'evaluation'    =>  'AND',
            'operation'     =>  $operation
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param array $values the values to look through
     * @return IPredicate
     */
    public function OrNotIn($name, $values)
    {
        $operation = $name.' NOT IN (';
        foreach($values as $v)
            $operation .= $v.',';
        $operation = rtrim($operation,',');
        $operation .= ')';
        $this->QueryBuilder([
            'evaluation'    =>  'OR',
            'operation'     =>  $operation
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param int $const indicates how the comparison should take place
     * @param string $value the value on which the operation is executed
     * @return IPredicate
     */
    public function Like($name, $const, $value)
    {
        $operation = $name.' LIKE ';
        switch($const)
        {
            case self::STARTS_WITH  : { $operation .= '%'.$value; break; }
            case self::ENDS_WITH    : { $operation .= $value.'%'; break; }
            case self::CONTAINS     : { $operation .= '%'.$value.'%'; break; }
            default                 : { $operation .= '%'.$value.'%'; break; }
        }
        $this->QueryBuilder([
            'evaluation'    =>  'AND',
            'operation'     =>  $operation
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param int $const indicates how the comparison should take place
     * @param string $value the value on which the operation is executed
     * @return IPredicate
     */
    public function OrLike($name, $const, $value)
    {
        $operation = $name.' LIKE ';
        switch($const)
        {
            case self::STARTS_WITH  : { $operation .= '%'.$value; break; }
            case self::ENDS_WITH    : { $operation .= $value.'%'; break; }
            case self::CONTAINS     : { $operation .= '%'.$value.'%'; break; }
            default                 : { $operation .= '%'.$value.'%'; break; }
        }
        $this->QueryBuilder([
            'evaluation'    =>  'OR',
            'operation'     =>  $operation
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param int $const indicates how the comparison should take place
     * @param string $value the value on which the operation is executed
     * @return IPredicate
     */
    public function NotLike($name, $const, $value)
    {
        $operation = $name.' NOT LIKE ';
        switch($const)
        {
            case self::STARTS_WITH  : { $operation .= '%'.$value; break; }
            case self::ENDS_WITH    : { $operation .= $value.'%'; break; }
            case self::CONTAINS     : { $operation .= '%'.$value.'%'; break; }
            default                 : { $operation .= '%'.$value.'%'; break; }
        }
        $this->QueryBuilder([
            'evaluation'    =>  'AND',
            'operation'     =>  $operation
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param int $const indicates how the comparison should take place
     * @param string $value the value on which the operation is executed
     * @return IPredicate
     */
    public function OrNotLike($name, $const, $value)
    {
        $operation = $name.' NOT LIKE ';
        switch($const)
        {
            case self::STARTS_WITH  : { $operation .= '%'.$value; break; }
            case self::ENDS_WITH    : { $operation .= $value.'%'; break; }
            case self::CONTAINS     : { $operation .= '%'.$value.'%'; break; }
            default                 : { $operation .= '%'.$value.'%'; break; }
        }
        $this->QueryBuilder([
            'evaluation'    =>  'OR',
            'operation'     =>  $operation
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param string $operation the operation signature eg(=,<,>,etc...)
     * @param string $value the value to apply the operation to
     * @return IPredicate
     */
    public function Having($name, $operation, $value)
    {
        $this->QueryBuilder([
            'evaluation'        =>  'AND',
            'operation'         =>  'HAVING '.$name.' '.$operation.' '.$value
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param string $operation the operation signature eg(=,<,>,etc...)
     * @param string $value the value to apply the operation to
     * @return IPredicate
     */
    public function OrHaving($name, $operation, $value)
    {
        $this->QueryBuilder([
            'evaluation'        =>  'OR',
            'operation'         =>  'HAVING '.$name.' '.$operation.' '.$value
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param string $val1 the first value of comparison
     * @param string $val2 the second value of comparison
     * @return IPredicate
     */
    public function Between($name, $val1, $val2)
    {
        $this->QueryBuilder([
            'evaluation'    =>  'AND',
            'operation'     =>  $name.' BETWEEN '.$val1.' AND '.$val2
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param string $val1 the first value of comparison
     * @param string $val2 the second value of comparison
     * @return IPredicate
     */
    public function NotBetween($name, $val1, $val2)
    {
        $this->QueryBuilder([
            'evaluation'    =>  'AND',
            'operation'     =>  $name.' NOT BETWEEN '.$val1.' AND '.$val2
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param string $val1 the first value of comparison
     * @param string $val2 the second value of comparison
     * @return IPredicate
     */
    public function OrBetween($name, $val1, $val2)
    {
        $this->QueryBuilder([
            'evaluation'    =>  'OR',
            'operation'     =>  $name.' BETWEEN '.$val1.' AND '.$val2
        ]);
        return $this;
    }

    /**
     * @param string $name the name of the field
     * @param string $val1 the first value of comparison
     * @param string $val2 the second value of comparison
     * @return IPredicate
     */
    public function OrNotBetween($name, $val1, $val2)
    {
        $this->QueryBuilder([
            'evaluation'    =>  'OR',
            'operation'     =>  $name.' NOT BETWEEN '.$val1.' AND '.$val2
        ]);
        return $this;
    }

    /**
     * @return IPredicate
     */
    public function StartGroup()
    {
        // increment counter so that we use a higher array index
        $this->GroupActive++;

        // initialize the active group in a null state so that
        // leaf nodes and parents can be aligned properly
        $this->Group["$this->GroupActive"] = null;
        return $this;
    }

    /**
     * @return IPredicate
     */
    public function EndGroup()
    {
        //get the current array index
        $current_group = $this->GroupActive;

        //if it is zero, this function was called without
        // $this->StartGroup() called first
        //we simply return the instance of the calss
        if($current_group === 0)
        {
            return $this;
        }

        //We decrement the active counter and ad a
        // stub to the query builder. When building
        // the expression the stub will point to the
        // group that should be evaluated
        $this->GroupActive--;
        $this->QueryBuilder([
            'evaluation'    =>  'node',
            'operation'     =>  $current_group
        ]);
        return $this;
    }

    /**
     * Returns the generated expression in a string that can be parsed by
     * The repository.
     * @return string
     */
    public function PredicateExpression()
    {
        $yield = $this->Root->accept(new YieldVisitor());
        $this->BuildPredicateString($yield);
        return $this->PredicateString;
    }

    /**
     * @param NodeInterface[] $node
     * @return void
     */
    protected function BuildPredicateString($node)
    {
        for($i=0; $i<count($node); $i++)
        {
            $node_value = $node[$i]->getValue();
            if(substr($node_value,0,4) === 'NODE')
            {
                $this->PredicateString .= '(';
                $index = explode('|',$node_value);
                $new_node = $this->Group[$index[1]];
                if(!empty($new_node->getChildren()))
                {
                    $this->BuildPredicateString($new_node->getChildren());
                }
                else
                {
                    $this->PredicateString .= $new_node->getValue();
                }

                $this->PredicateString .= ($i === (count($node) - 1))
                    ? ')'
                    : ') '.$node[$i]->getParent()->getValue().' '
                ;
            }
            else
            {
                $this->PredicateString .= ($i === (count($node) - 1))
                    ? $node_value
                    : $node_value.' '.$node[$i]->getParent()->getValue().' '
                ;
            }
        }
    }
}
<?php

namespace Axium\SDK\Services\Utilities;
use Axium\SDK\Interfaces\IResponseCode;

class ResponseCode implements IResponseCode
{

    /**
     * @param int $code response code
     * @return bool
     */
    public static function IsOK($CODE)
    {
        return ($CODE >= 200 && $CODE < 300) ? true : false;
    }

    /**
     * @param int $code response code
     * @return bool
     */
    public static function IsNotFound($CODE)
    {
        return ($CODE >= 400 && $CODE < 500) ? true : false;
    }

    /**
     * @param int $code response code
     * @return bool
     */
    public static function IsNotModified($CODE)
    {
        return ($CODE >= 300 && $CODE < 400) ? true : false;
    }

    /**
     * @param int $code response code
     * @return bool
     */
    public static function IsInternalError($CODE)
    {
        return ($CODE >= 500 && $CODE < 600) ? true : false;
    }

    /**
     * @param int $code response code
     * @return bool
     */
    public static function HasNoResult($CODE)
    {
        return ($CODE === 600) ? true : false;
    }
}
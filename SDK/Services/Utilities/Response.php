<?php

namespace Axium\SDK\Services\Utilities;
use Axium\SDK\Interfaces\IResponse;
use Axium\SDK\Models\RepositoryModel;
use Axium\SDK\Models\ResponseModel;
use Axium\SDK\Models\SelectListModel;

class Response implements IResponse
{
    /**
     * @var ResponseModel model which contains all
     * the data the response will use
     */
    protected $responseModel;

    /**
     * Response constructor.
     * @param ResponseModel $model
     */
    public function __construct(ResponseModel $model)
    {
        $this->responseModel = $model;
    }

    /**
     * @return int status of the response
     */
    public function GetStatus()
    {
        return $this->responseModel->Status;
    }

    /**
     * @return string[] list of the errors generated
     * while the repository executed
     */
    public function ErrorList()
    {
        return $this->responseModel->Errors;
    }

    /**
     * @param int $id index of the record to be returned (1 being the first index)
     * @return object
     */
    public function GetRecord($id=1)
    {
        $index = ($id - 1);
        return (isset($this->responseModel->Models[$index]))
            ? $this->responseModel->Models[$index]
            : null
        ;
    }

    /**
     * @return object[] list of all the objects generated by the repository
     */
    public function GetRecordList()
    {
        return $this->responseModel->Models;
    }

    /**
     * @return int total number of records retrieved
     */
    public function GetRecordCount()
    {
        return count($this->responseModel->Models);
    }

    /**
     * @return string JSON encode the response
     */
    public function Serialize()
    {
        return json_encode($this->responseModel,true);
    }

    /**
     * @param string $result serialized response from api call
     * @param RepositoryModel $model model that should be used when converting the response to an object
     * @param array $mapper if model parameters are different from the response result... add a mapper
     * @return ResponseModel
     */
    public static function UnPack($result,$r_model,$map=null)
    {
        $res = json_decode($result,true);
        $model = new ResponseModel();

        if(is_array($res))
        {
            $model->Success = (int)$res['Success'];
            $model->Errors = $res['Errors'];
            $model->Message = $res['Message'];
            if(!empty($res['Models']))
            {
                foreach($res['Models'] as $m)
                {
                    $r = clone $r_model;
                    foreach($m as $key => $val)
                    {
                        if(!is_null($map))
                        {
                            $ar = array_flip($map);
                            if(array_key_exists($key,$ar))
                                $r->{$ar[$key]} = $val;
                            else
                                $r->{$key} = $val;
                            continue;
                        }
                        $r->{$key} = $val;
                    }
                    $model->Models[] = $r;
                }
            }
        }
        else
        {
            $model->Success = (int)$res->Success;
            $model->Errors = $res->Errors;
            $model->Message = $res->Message;
            if(!empty($res->Models))
            {
                foreach($res->Models as $m)
                {
                    $r = clone $r_model;
                    foreach($m as $key => $val)
                    {
                        if(!is_null($map))
                        {
                            $ar = array_flip($map);
                            if(array_key_exists($key,$ar))
                                $r->{$ar[$key]} = $val;
                            else
                                $r->{$key} = $val;
                            continue;
                        }
                        $r->{$key} = $val;
                    }
                    $model->Models[] = $r;
                }
            }
        }
        return $model;
    }

    /**
     * @param string $key identifies the parameter to be used as the value
     * @param string $val identifies the parameter to be used as the text
     * @return object[] returns an array of key value objects eg. [{value:'',text:''},{value:'',text:''}]
     */
    public function GetDropDownList($key = 'ID', $val = 'Name')
    {
        $dropDownList = [];
        if(!empty($this->responseModel->Models))
        {
            foreach($this->responseModel->Models as $model)
            {
                if(property_exists($model,$key) && property_exists($model,$val))
                {
                    $entity = new SelectListModel();
                    $entity->Value = $model->{$key};
                    $entity->Text = $model->{$val};
                    $dropDownList[] = $entity;
                }
            }
        }
        return $dropDownList;
    }
}
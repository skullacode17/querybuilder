<?php


namespace Axium\SDK\Models;


class ResultModel
{
    /**
     * @var boolean Stores the status to be sent to the UI
     */
    public $Success;

    /**
     * @var string Stores the message to be sent to the UI
     */
    public $Message;

    /**
     * @var object Stores the models to be sent to the UI
     */
    public $Model;

    /**
     * @var object[] Stores the list of models to be sent to the UI
     */
    public $List;

    /**
     * @var string[] Stores the errors to be sent the UI
     */
    public $Errors;

    public function __construct()
    {
        $this->Model = [];
        $this->Message = '';
        $this->Success = false;
        $this->Errors = [];
        $this->List = [];
    }
}
<?php


namespace Axium\SDK\Models;


use Axium\SDK\Services\Utilities\ResponseCode;

class ResponseModel
{
    /**
     * @var int Stores the response code from the repository execution
     */
    public $Status;

    /**
     * @var string[] Stores the list of errors from the repository execution
     */
    public $Errors;

    /**
     * @var object[] Stores the models generated from the repository execution
     */
    public $Models;

    /**
     * ResponseModel constructor.
     */
    public function __construct()
    {
        $this->Status = ResponseCode::NOT_MODIFIED;
        $this->Errors = [];
        $this->Models = [];
    }
}
<?php


namespace Axium\SDK\Models;


use Axium\SDK\Services\Utilities\ResponseCode;

class DataSourceModel
{
    /**
     * @var string identifies the type of repository
     */
    public $Type;

    /**
     * @var string the current version of the
     * repository and/or the underlining driver
     */
    public $Version;

    /**
     * @var int determines the current state
     * of the repository connection
     */
    public $Status;

    /**
     * DataSourceModel constructor.
     */
    public function __construct()
    {
        $this->Type = '';
        $this->Version = '';
        $this->Status = ResponseCode::OK;
    }
}
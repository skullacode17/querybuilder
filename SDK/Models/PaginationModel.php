<?php


namespace Axium\SDK\Models;


class PaginationModel
{
    /**
     * @var int The group of results to be considered
     */
    public $Page;

    /**
     * @var int The amount of results to return in the group
     */
    public $Limit;

    public function __construct()
    {
        $this->Page = 1;
        $this->Limit = 5;
    }
}
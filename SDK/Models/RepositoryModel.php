<?php


namespace Axium\SDK\Models;


class RepositoryModel
{
    /**
     * @var string Primary Key
     */
    public $ID;

    /**
     * @var string name of the entity
     */
    public $Name;

    public function __construct()
    {
        $this->ID = '';
        $this->Name = '';
    }
}
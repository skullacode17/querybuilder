<?php

namespace Axium\SDK\Models;


use Axium\SDK\Interfaces\IRequestModel;
use ReflectionObject;
use ReflectionProperty;

class RequestModel implements IRequestModel
{
    /**
     * @var string[] Stores errors
     */
    protected $errorList;

    /**
     * @var array Stores models added to the request
     */
    protected $models;

    /**
     * @var array Stores lists of models added to the request
     */
    protected $lists;

    /**
     * @var bool Stores the success value for the request
     */
    protected $success;

    /**
     * @var string Stores the message for the request
     */
    protected $message;

    public function __construct()
    {
        $this->errorList = [];
        $this->message = '';
        $this->success = false;
        $this->lists = [];
        $this->models = [];
    }

    /**
     * Function should be overwritten by inheriting class
     * @return bool determines if the validations were passed
     */
    public function IsValid()
    {
        return true;
    }

    /**
     * @return string[] list of errors accumulated by validator
     */
    public function ErrorList()
    {
        return $this->errorList;
    }

    /**
     * @param string $error error message
     * @return IRequestModel
     */
    public function AddError($error)
    {
        $this->errorList[] = $error;
        return $this;
    }

    /**
     * @param string $name identifier for the model that is added
     * @param object $model the model with the data
     * @return IRequestModel
     */
    public function AddModel($name, $model)
    {
        $this->models[$name] = $model;
        return $this;
    }

    /**
     * @param string $name identifier for the list of models added
     * @param array $list list of models
     * @return IRequestModel
     */
    public function AddList($name, $list)
    {
        $this->lists[$name] = $list;
        return $this;
    }

    /**
     * @return array returns public parameters as an
     * associative array
     */
    public function ToArray()
    {
        $result = [];
        $vars = (new ReflectionObject($this))->getProperties(ReflectionProperty::IS_PUBLIC);
        foreach($vars as $var)
            $result[$var->name] = $this->{$var->name};
        return $result;
    }

    /**
     * @return string returns the result a s a JSON encoded string
     */
    public function GetResultAndSerialize()
    {
        return json_encode($this->GetResult(),true);
    }

    /**
     * @return ResultModel returns the model to be passed to UI
     */
    public function GetResult()
    {
        $model = new ResultModel();
        $model->Success = $this->success;
        $model->Errors = $this->errorList;
        $model->Message = $this->message;
        $model->List = $this->lists;
        $model->Model = $this->models;
        return $model;
    }

    /**
     * @param bool $bool the status state
     * @return IRequestModel
     */
    public function SetSuccess($bool)
    {
        $this->success = $bool;
        return $this;
    }

    /**
     * @param string $message the message to display
     * @return IRequestModel
     */
    public function SetMessage($message)
    {
        $this->message = $message;
        return $this;
    }
}
<?php


namespace Axium\SDK\Models;


class SelectListModel
{
    /**
     * @var string represents the value of the entity
     */
    public $Value;

    /**
     * @var string represents the name of the entity
     */
    public $Text;
}
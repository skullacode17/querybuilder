<?php


namespace Axium\SDK\Models;


class OrderModel
{
    /**
     *
     */
    const ORDER_ASCENDING = 'asc';

    /**
     *
     */
    const ORDER_DESCENDING = 'desc';

    /**
     * @var string Name of the field to order by
     */
    public $Column;

    /**
     * @var string Direction that should be considered. constants are provided
     */
    public $Direction;

    public function __construct()
    {
        $this->Column = 'ID';
        $this->Direction = self::ORDER_ASCENDING;
    }
}